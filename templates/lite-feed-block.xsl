<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes" indent="no" method="html" />
  <xsl:variable name="block_id">default</xsl:variable>
  <xsl:variable name="item_limit">3</xsl:variable>
  <xsl:template match="/rss/channel">
    <xsl:element name="div">
      <xsl:attribute name="class">
        lite-feed-block lite-feed-block-<xsl:value-of select="$block_id" />
      </xsl:attribute>
      <xsl:for-each select="item[position() &lt;= $item_limit]">
        <div class="lite-feed-block-item">
          <h3 class="lite-feed-block-item-title">
            <xsl:element name="a">
              <xsl:attribute name="href">
                <xsl:value-of select="link" />
              </xsl:attribute>
              <xsl:value-of select="title" />
            </xsl:element>
          </h3>
          <div class="lite-feed-block-item-description">
            <xsl:value-of select="description" disable-output-escaping="yes" />
          </div>
        </div>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>

