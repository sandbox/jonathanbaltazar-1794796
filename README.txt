REQUIREMENTS
------------
This module requires the PHP extension php_xsl.

INSTALLATION
------------
After enabling the Lite Feed Block module, a sample block will be available in 
the Drupal blocks list. This block can be deleted if necessary.

CONFIGURATION
-------------
New lite feed blocks can be added through the menu item 
admin/structure/block/lite-feed-block-add. Lite feed blocks can be configured 
and deleted using the standard block configuration interface.

TEMPLATES
---------
The display of lite feed blocks can be customized using the versatility of XSLT.
General custom templates can be created for all lite feed blocks, and specific 
templates can be created for individual lite feed blocks. The default template, 
lite_feed_block/templates/lite-feed-block.xsl, can be copied into the theme's 
templates folder and modified as needed. The most specific template will be 
selected first. An overview of the naming conventions for templates is listed 
below:

* Specific template for a lite feed block
  [path_to_theme]/templates/lite-feed-block--[lite-feed-block-id].xsl
  
* General template for all lite feed blocks
  [path_to_theme]/templates/lite-feed-block.xsl